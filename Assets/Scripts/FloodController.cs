﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloodController : MonoBehaviour
{
    private static FloodController instance;
    private static Vector3 direction = Vector3.up;
    private const float StopThreshold = 0.01F;
    public const float FinalTarget = 74f;
    public Vector3 currentLevel { get; private set; }
    public bool enabled = true;
    public float velocity = 0.2f;
    public float targetLevel = FinalTarget;

      private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    private void Start(){
    }


    public static FloodController GetInstance()
    {
        return instance;
    }
    private void FixedUpdate()
    {
        Debug.Log(currentLevel);
        if (!enabled)
        {
            return;
        }
        Vector3 destination = direction * targetLevel;
        currentLevel = new Vector3(0,transform.position.y,0);
        if (Vector3.Distance(currentLevel, destination) > StopThreshold)
        {
            Vector3 heading = (destination - currentLevel).normalized * velocity * Time.fixedDeltaTime;
            transform.position += heading;
            //transform.localScale += heading;
        }
        else if(targetLevel!=FinalTarget){
            velocity=0.2f;
            targetLevel=FinalTarget;
        }
    }

    public void Drain(){
        targetLevel=currentLevel.y-18f;
    }

}
