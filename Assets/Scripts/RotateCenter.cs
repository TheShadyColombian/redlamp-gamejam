﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCenter : MonoBehaviour
{

    public GameObject camera;

    public Transform cameraRig;
    public GameObject cameraIsometric;
    public Transform cameraAnchor;
    public int floor;
    public Transform floorTransform;
    public Transform aditionalFLoor;
    public bool invertAdditional;
    public float speed;
    public Vector3 offset;
    public float offsetView = -1f;
    private PlayerController player;
    private Quaternion rotateTo;
    private float dir;
    private bool selected = false;

    // Start is called before the first frame update
    void Start()
    {

        rotateTo = floorTransform.rotation;
        player = PlayerController.GetInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if(selected){
            if (player.canRotate && Input.GetKeyDown("e"))
            {
                //camera rotation
                float y = cameraRig.rotation.eulerAngles.y;
                Quaternion rot = new Quaternion(0f, 0f, 0f, 0f);
                rot.eulerAngles = new Vector3(30f, (y <= 90f ? 225f : 45f), 0f);
                cameraAnchor.rotation = rot;
                //floor position
                cameraAnchor.position = new Vector3(0f, 16 * Mathf.Max(floor - 1+offsetView, 0f), 0f);
                player.isRotating = !player.isRotating;
                if (player.isRotating)
                {
                    camera.SetActive(false);
                    cameraIsometric.SetActive(true);
                }
                else
                {
                    camera.SetActive(true);
                    cameraIsometric.SetActive(false);
                }
            }
            dir = 0f;
            if (player.isRotating && Input.GetKeyDown("d"))
                dir = -1f;
            else if (player.isRotating && Input.GetKeyDown("a"))
                dir = 1f;
            rotateTo.eulerAngles = new Vector3(0f, rotateTo.eulerAngles.y + (90f * dir), 0f) + offset;
            floorTransform.rotation = Quaternion.Lerp(floorTransform.rotation, rotateTo, Time.deltaTime * speed);
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Debug.Log("enter tower");
            selected = true;
            player.canRotate = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("out tower");
            selected = false;
            player.canRotate = false;
        }
    }
}
