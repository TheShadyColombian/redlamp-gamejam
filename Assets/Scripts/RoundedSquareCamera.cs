﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RoundedSquareCamera : MonoBehaviour
{

    public Transform target;
    public float radius = 1;
    public float lerpSpeed;

    private Transform camera;

    void Start()
    {
        camera = FindObjectOfType<CameraRig>().transform;
    }

    void FixedUpdate() {
        transform.position = TopDownClampValues(target.position, radius);
        Vector3 delta = target.position - transform.position;
        transform.rotation = Quaternion.Euler(0, Mathf.Atan2(delta.x, delta.z) * Mathf.Rad2Deg, 0);
        camera.transform.position = Vector3.Lerp(camera.transform.position, transform.GetChild(0).position, Time.fixedDeltaTime * lerpSpeed);
        camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, transform.GetChild(0).rotation, Time.fixedDeltaTime * lerpSpeed);
    }

    public static Vector3 TopDownClampValues(Vector3 vector, float max)
    {
        //Store the positive/negative values of each axis here (as the following operation requires absolute values)
        Vector3 polarity = new Vector3(vector.x > 0 ? 1 : -1, 1, vector.z > 0 ? 1 : -1);
        return Vector3.Scale(new Vector3(Mathf.Min(Mathf.Abs(vector.x), max), vector.y, Mathf.Min(Mathf.Abs(vector.z), max)), polarity);
    }

}