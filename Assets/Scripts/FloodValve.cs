﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloodValve : MonoBehaviour {

    public float setFloodLevelTo;

    public TextMeshPro setToDisplay;
    public TextMeshPro currentLevelDisplay;

    private Collider player;
    private FloodController controller;
    private bool useable;
    
    // Start is called before the first frame update
    void Start() {

        player = FindObjectOfType<PlayerController>().GetComponent<Collider>();

        controller = FindObjectOfType<FloodController>();

        if (setToDisplay)
            setToDisplay.text = setFloodLevelTo.ToString();

    }

    // Update is called once per frame
    void Update() {

        if (currentLevelDisplay)
            currentLevelDisplay.text = (Mathf.Round(controller.currentLevel.y * 10) * 0.1f).ToString();

        if (useable && Input.GetButtonDown("Interact"))
            controller.targetLevel = setFloodLevelTo;

    }

    private void OnTriggerEnter(Collider other) {
        if (other == player)
            useable = true;
    }

    private void OnTriggerExit(Collider other) {
        if (other == player)
            useable = false;
    }

}