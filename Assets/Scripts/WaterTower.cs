﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterTower : MonoBehaviour
{

    public GameObject camera;
    public Transform cameraRig;
    public GameObject cameraIsometric;
    public Transform cameraAnchor;
    public float floor;

    private PlayerController player;
    private FloodController water;
    private bool selected = false;
    private Reloj time;

    private static float reload = 10f;
    private float activated;

    public GameObject sphere;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerController.GetInstance();
        water = FloodController.GetInstance();
        time = Reloj.GetInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if (selected)
        {
            if (player.canRotate && Input.GetKeyDown("e") && activated == 0)
            {
                //camera rotation
                float y = cameraRig.rotation.eulerAngles.y;
                Quaternion rot = new Quaternion(0f, 0f, 0f, 0f);
                rot.eulerAngles = new Vector3(30f, (y <= 90f ? 225f : 45f), 0f);
                cameraAnchor.rotation = rot;
                //floor position
                cameraAnchor.position = new Vector3(0f, 16 * Mathf.Max(floor - 2, 0f), 0f);
                player.isRotating = true;
                camera.SetActive(false);
                cameraIsometric.SetActive(true);
                activated = time.tiempoMostrarEnSegundos;
                sphere.SetActive(false);
                water.Drain();
                water.velocity = 5;
            }

            if (water.targetLevel == FloodController.FinalTarget)
            {
                camera.SetActive(true);
                cameraIsometric.SetActive(false);
                player.isRotating = false;
            }
        }

        if (time.tiempoMostrarEnSegundos - activated > reload)
        {
            activated = 0;
            sphere.SetActive(true);
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            Debug.Log("enter tower");
            selected = true;
            player.canRotate = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("out tower");
            selected = false;
            player.canRotate = false;
        }
    }
}
