﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[ExecuteInEditMode]
public class LadderController : MonoBehaviour
{
    public enum Facing
    {
        North, South, East, West
    };

    public const float LadderSectionSize = 2F;

    //To improve
    public BoxCollider ladderPhysicColider;
    public BoxCollider ladderColider;
    public GameObject ladderBody;
    public ResizeableLadder ladder;
    public Facing facing = Facing.North;
    [Min(1)]
    public int length = 2;
    public bool snap = true;

    void Update()
    {
        //Set the size
        float verticalSize = length * LadderSectionSize;
        //Ladder body
        ladderColider.size = new Vector3(0.6F, verticalSize - 0.2F, 1F);
        ladderColider.center = new Vector3(0.3F, -0.4F, 0);

        ladderPhysicColider.size = new Vector3(0.4F, verticalSize, 1F);
        ladderPhysicColider.center = new Vector3(0.8F, -0.5F, 0);

        ladderBody.transform.localPosition = new Vector3(1F, -((verticalSize / 2F)) + 0.5F, 0);

        Vector3 facingVector = GetFacingVector(facing);
        //Set the rotation
        transform.rotation = Quaternion.LookRotation(facingVector);
        //Update ladder size
        ladder.length = length;
        //Snap to grid
        if (snap)
        {
            transform.localPosition = Vector3Int.RoundToInt(transform.localPosition);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            playerController.setMovementContext(null);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        /*PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            playerController.setMovementContext(null);
        }*/
    }

    public static Vector3 GetFacingVector(Facing facing)
    {
        switch (facing)
        {
            case Facing.North:
                return Vector3.left;
            case Facing.South:
                return Vector3.right;
            case Facing.East:
                return Vector3.forward;
            case Facing.West:
                return Vector3.back;
            default:
                return Vector3.zero;
        }
    }

    public static Vector3 GetContextVector(Facing facing)
    {
        switch (facing)
        {
            case Facing.North:
                return Vector3.forward;
            case Facing.South:
                return Vector3.back;
            case Facing.East:
                return Vector3.right;
            case Facing.West:
                return Vector3.left;
            default:
                return Vector3.zero;
        }
    }

}