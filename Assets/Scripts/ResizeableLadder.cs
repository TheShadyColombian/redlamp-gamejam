﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[ExecuteInEditMode]
public class ResizeableLadder : MonoBehaviour
{
    [Min(0)]
    public int length = 0;
    public GameObject ladderSectionPrefab;
    private GameObject[] ladderSections = new GameObject[0];
    private int lastLength = 0;
    private float sectionSize = LadderController.LadderSectionSize;

    private void Awake() {
        DeleteSections();
    }

    private void InstanciatePrefabs() {
        GameObject[] previousSections = ladderSections;
        //Clear the last prefabs if the new length is less than the previous one
        for (int i = previousSections.Length - 1; i >= length; i--)
        {
            if (Application.isPlaying) {
                Destroy(previousSections[i]);
            } else {
                DestroyImmediate(previousSections[i]);
            }   
        }
        ladderSections = new GameObject[length];
        //Copy the prefabs to the new array
        for (int i = 0; i < ladderSections.Length && i < previousSections.Length; i++)
        {
            ladderSections[i] = previousSections[i];   
        }
        //Instanciate the new sections 
        for (int i = previousSections.Length; i < ladderSections.Length; i++)
        {
            if (Application.isPlaying) {
                ladderSections[i] = Instantiate(ladderSectionPrefab);
            }
            else {
                #if UNITY_EDITOR
                    ladderSections[i] = PrefabUtility.InstantiatePrefab(ladderSectionPrefab) as GameObject;
                #endif
            }
            ladderSections[i].transform.parent = transform;
            ladderSections[i].transform.localRotation = Quaternion.identity;
            ladderSections[i].transform.localPosition = Vector3.up * sectionSize * i; 
        }
        lastLength = ladderSections.Length;
    }

    private void Resize() {
        for (int i = 0; i < ladderSections.Length; i++)
        {
            ladderSections[i].transform.localPosition = Vector3.up * sectionSize * i; 
        }
    }

    private void DeleteSections() {
        //Delete all prefabs generated
        foreach (Transform child in transform)
        {
            if (Application.isPlaying) {
                Destroy(child.gameObject);
            } else {
                DestroyImmediate(child.gameObject);
            }  
         }
        ladderSections = new GameObject[0];
    }

    private void OnEnable()
    {
        if(lastLength != ladderSections.Length) {
             InstanciatePrefabs();
        }
    }

    private void OnDisable()
    {
        DeleteSections();
    }

    private void Update()
    {
        if(lastLength != length) {
            InstanciatePrefabs();
        }
        if(sectionSize != LadderController.LadderSectionSize) {
          sectionSize = LadderController.LadderSectionSize;
          Resize();
        }
    }

}