﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public bool taken;
    private void OnTriggerEnter(Collider other)
    {
        PlayerController playerController = other.gameObject.GetComponent<PlayerController>();
        if (playerController != null && other is BoxCollider)
        {
            if (!taken)
            {
                playerController.setLastCheckpoint(this);
                taken = true;
            }
        }
    }

    public Vector3 getSpawnPoint()
    {
        return transform.position;
    }

}
