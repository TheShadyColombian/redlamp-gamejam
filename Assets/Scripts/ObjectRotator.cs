﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    public float rotationSpeed = 1F;
    public Vector3 rotationAxis = Vector3.up;

    void Update()
    {
        transform.Rotate(rotationAxis.x * rotationSpeed, rotationAxis.y * rotationSpeed, rotationAxis.z * rotationSpeed, Space.Self);
    }
}