﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    [Header ("Movement Parameters")]
    private MovementContext movementContext;
    private RotationContext rotationContext;


    private static PlayerController instance;

    public LayerMask water;
    public LayerMask ground;
    public GameObject deathView;
    
    private Rigidbody rigidbody;
    public Animator animator;
    private GroundCheck groundCheck;
    
    private bool inAir;
    private bool inJump;
    private bool inTrack;


    private Vector3 velocity;
    private Vector3 lasthHorizontalDirection;

    private CheckPoint lastCheckpoint;

    private float timeUnderWater;
    private float highestFallDistance;
    public int deaths;

    public float movementAcceleration = 20F;
    public float movementSpeedCap = 20f;
    public float horizontalDrag = 2f;
    public float jumpSpeed = 9F;
    public float doubleJumpForce = 9F;
    public float airHorizontalGain = 0.3F;
    public float fallDeathDistance = 10F;
    public float underwaterDeathTime = 5F;
    public bool canRotate;
    public bool isRotating;

    private ParticleSystem walkParticleEmitter;
    private ParticleSystem.EmissionModule walkParticleEmitterModule;
    private ParticleSystem landParticleEmitter;
    private ParticleSystem jumpParticleEmitter;
    private float lastLanded;
    private bool softLanded;
    private bool _softLanded;

    private bool died;
    private Reloj reloj;

    private void Start()
    {
        reloj= Reloj.GetInstance();
        rigidbody = GetComponent<Rigidbody>();
        groundCheck = GetComponentInChildren<GroundCheck>();
        walkParticleEmitter = GetComponentInChildren<ParticleSystem>();
        walkParticleEmitterModule = walkParticleEmitter.emission;
        landParticleEmitter = walkParticleEmitter.GetComponentsInChildren<ParticleSystem>()[1];
        jumpParticleEmitter = walkParticleEmitter.GetComponentsInChildren<ParticleSystem>()[2];

    }

     private void Awake(){
        if (instance == null){
            instance = this;
        }
    }


    public static PlayerController GetInstance(){
        return instance;
    }


    private void Update() {
        if (groundCheck.grounded) {
            lastLanded = Time.time;
            softLanded = true;
        }

        if (Time.time - lastLanded > 0.05f) {
            softLanded = false;
        }
        if (softLanded && !_softLanded) {
            walkParticleEmitterModule.enabled = true;
            landParticleEmitter.Play();
        }
        if (!softLanded)
            walkParticleEmitterModule.enabled = false;
        

        if(!isRotating && !died){
            animator.SetFloat("speed", Vector3.Scale(rigidbody.velocity, new Vector3(1, 0, 1)).magnitude);
            velocity = Vector3.zero;
            if (groundCheck.grounded) {
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W)) {
                    rigidbody.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
                    animator.SetBool("grounded", false);
                    animator.SetTrigger("jump");
                    landParticleEmitter.Play(true);
                    inJump = true;
                }
                if (inAir)
                {
                    if (highestFallDistance - transform.position.y >= fallDeathDistance)
                    {
                        if(transform.position.y < -1f)
                            FullDie();
                        else
                            Die();
                    }
                }
                highestFallDistance = transform.position.y;
                inAir = false;
            } 
            else {
                if (transform.position.y > highestFallDistance)
                {
                    highestFallDistance = transform.position.y;
                }
                if (inJump) {
                    //Doble jump control
                    if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W)) {
                        rigidbody.velocity = (Vector3.up * doubleJumpForce) + (new Vector3(rigidbody.velocity.x, 0, rigidbody.velocity.z));
                        animator.ResetTrigger("jump");
                        jumpParticleEmitter.Play(true);
                        inJump = false;
                    }
                }
            inAir = true;

            }
            float horizontalInput = Input.GetAxis("Horizontal");
            //Context movement
            if (movementContext != null)
            {
                velocity += movementContext.direction * horizontalInput * movementAcceleration;
                if (!groundCheck.grounded)
                {
                    velocity *= airHorizontalGain;
                }
                if (horizontalInput != 0) {
                    lasthHorizontalDirection = (movementContext.direction * horizontalInput).normalized;
                }
                //Center the player in the context
                Vector3 positionXUnitary = new Vector3(transform.position.x, 0, 0).normalized;
                bool perpendicular = Vector3.Dot(movementContext.direction.normalized, positionXUnitary) == 0;
                float x = perpendicular ? movementContext.transform.position.x : transform.position.x;
                float y = transform.position.y;
                float z = !perpendicular ? movementContext.transform.position.z : transform.position.z;
                //Horizontal contexts
                if (movementContext.direction.y == 0)
                {
                    if (horizontalInput != 0)
                    {
                        lasthHorizontalDirection = (movementContext.direction * horizontalInput).normalized;
                    }
                    rigidbody.useGravity = true;
                }
                //Vertical context
                else
                {
                    rigidbody.useGravity = false;
                    lasthHorizontalDirection.y = 0;
                }
                transform.position = new Vector3(x, y, z);
            }
            else
            {
                lasthHorizontalDirection = transform.forward;
            }
        }
        //Animations
        animator.SetBool("grounded", groundCheck.grounded);
        animator.SetFloat("fallSpeed", -rigidbody.velocity.y);

        _softLanded = softLanded;

    }

    private void FixedUpdate()
    {
        //Move the player
        rigidbody.velocity += velocity * Time.fixedDeltaTime;

        //Clip player speed
        Vector3 vel = RoundedSquareCamera.TopDownClampValues(rigidbody.velocity, movementSpeedCap);
        if (groundCheck.grounded || velocity.magnitude < 0.1f) {
            vel.x /= 1 + (horizontalDrag * Time.unscaledDeltaTime);
            vel.z /= 1 + (horizontalDrag * Time.unscaledDeltaTime);
        }
        rigidbody.velocity = vel;

        //Rotate the player 
        transform.rotation = Quaternion.LookRotation(lasthHorizontalDirection);
    }

    public void Die()
    {
        if(!died){
        animator.SetTrigger("die");
        if (lastCheckpoint != null)
        {
            transform.position = lastCheckpoint.getSpawnPoint() + new Vector3(0, 1.8F, 0);
        }
        deaths++;
        timeUnderWater=0;
        }
    }

    public void setLastCheckpoint(CheckPoint lastCheckpoint)
    {
        this.lastCheckpoint = lastCheckpoint;
    }


    public void setMovementContext(MovementContext context)
    {
        this.movementContext = context;
    }

    private void OnTriggerEnter(Collider other)
    {
        RotationContext rotationContext = other.gameObject.GetComponent<RotationContext>();
        if (rotationContext != null)
        {
            this.rotationContext = rotationContext;
        }
        MovementContext movementContext = other.gameObject.GetComponent<MovementContext>();
        if (movementContext != null)
        {
            inTrack = true;
            setMovementContext(movementContext);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        MovementContext movementContext = other.gameObject.GetComponent<MovementContext>();
        if (movementContext != null)
        {
            inTrack = false;
            if (this.rotationContext != null)
            {
                MovementContext targetContext = movementContext == this.rotationContext.from ? this.rotationContext.to : this.rotationContext.from;
                setMovementContext(targetContext);
            }
        }
        RotationContext rotationContext = other.gameObject.GetComponent<RotationContext>();
        if (rotationContext != null)
        {
            if (!inTrack)
            {
                transform.position = new Vector3(rotationContext.transform.position.x, transform.position.y, rotationContext.transform.position.z);
            }
            this.rotationContext = null;
        }
         if (1 << other.gameObject.layer == water.value)
        {
            setMovementContext(null);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        MovementContext movementContext = other.gameObject.GetComponent<MovementContext>();
        if (movementContext != null)
        {
            if (this.movementContext == null)
            {
                setMovementContext(movementContext);
            }
        }
        //Stay in water
        if (1 << other.gameObject.layer == water.value)
        {
            CapsuleCollider capsule = GetComponentInChildren<CapsuleCollider>();
            if (other.bounds.Contains(capsule.bounds.min) && other.bounds.Contains(capsule.bounds.max))
            {
                timeUnderWater += Time.deltaTime;
            }
            else
            {
                if (timeUnderWater> 0)
                {
                    timeUnderWater = 0;
                }
            }
            if (timeUnderWater > underwaterDeathTime)
            {
                FullDie();
            }
        }
    }
    
    private void FullDie(){
        if(!died)
            deaths++;
        died=true;
        reloj.estaPausado = true;
        deathView.SetActive(true);
    }
}