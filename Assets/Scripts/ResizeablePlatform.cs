﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent (typeof (BoxCollider))]
public class ResizeablePlatform : MonoBehaviour {

    [Header ("Components")]
    public Transform endpointA;
    public Transform endpointB;
    [Header("Parameters")]
    public bool positionSnapping = true;
    public bool sizeSnapping = true;
    public Vector3 size;
    public Vector3 colliderSizeMulti;
    public Vector3 colliderSizeOffset;
    public Vector3 colliderPosMuti;
    public Vector3 colliderPosOffset;
    public Vector3 endpointAFromAxis;
    public Vector3 endpointAToAxis;
    public Vector3 endpointBFromAxis;
    public Vector3 endpointBToAxis;

    private Vector3 collCenter;
    private Vector3 collSize;

    private BoxCollider collider;

    // Start is called before the first frame update
    void Start() {

        TryGetComponent(out collider);

    }

    // Update is called once per frame
    void Update() {

        if (sizeSnapping)
            size = Vector3Int.RoundToInt(size);
        if (positionSnapping)
            transform.localPosition = Vector3Int.RoundToInt(transform.localPosition);

        collSize = AbsoluteVector (Vector3.Scale(colliderSizeMulti, size) + colliderSizeOffset);
        collCenter = Vector3.Scale(collSize, colliderPosMuti) + colliderPosOffset;

        collider.center = collCenter;
        collider.size = collSize;

        if (endpointA)
            endpointA.transform.localPosition = Vector3.Scale(endpointAFromAxis, size).magnitude * endpointAToAxis;
        if (endpointB)
            endpointB.transform.localPosition = Vector3.Scale(endpointBFromAxis, size).magnitude * endpointBToAxis;

    }

    public static Vector3 AbsoluteVector (Vector3 vector) {

        return new Vector3(Mathf.Abs(vector.x), Mathf.Abs(vector.y), Mathf.Abs(vector.z));

    }

}