﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTutorial : MonoBehaviour {

    public bool disableOnExit;
    [SerializeField]
    public GameObject tutorialToEnable;

    private Collider player;

    // Start is called before the first frame update
    void Start() {

        player = FindObjectOfType<PlayerController>().GetComponent<Collider>();
        tutorialToEnable.SetActive(false);
    }

    // Update is called once per frame
    void Update() {

    }

    void OnTriggerEnter(Collider other) {

        if (other == player)
            tutorialToEnable.SetActive(true);

    }

    void OnTriggerExit(Collider other) {

        if (other == player && disableOnExit)
            tutorialToEnable.SetActive(false);

    }

}