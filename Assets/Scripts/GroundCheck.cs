﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    public LayerMask ground;
    public Collider collider;
    public bool grounded;
    private List<Collider> collidersInContact = new List<Collider>();


    private void OnCollisionStay(Collision collision)
    {
        foreach (ContactPoint contactPoint in collision.contacts)
        {
            // Debug.Log(contactPoint.normal - Vector3.up);
            if (contactPoint.thisCollider != collider)
            {
                continue;
            }
            if (1 << collision.gameObject.layer == ground.value)
            {
                if (!collidersInContact.Contains(contactPoint.otherCollider))
                {
                    collidersInContact.Add(contactPoint.otherCollider);
                }
                grounded = true;
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (1 << collision.gameObject.layer == ground.value)
        {
            collidersInContact.RemoveAll(collider => collider.gameObject == collision.gameObject);
            if (collidersInContact.Count == 0)
            {
                grounded = false;
            }
        }
    }

}
