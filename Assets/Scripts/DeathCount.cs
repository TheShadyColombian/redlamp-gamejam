﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DeathCount : MonoBehaviour
{

    private PlayerController player;    
    private Text myText;
    // Start is called before the first frame update
    void Start()
    {
        player=PlayerController.GetInstance();
        myText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        myText.text = player.deaths.ToString("0");
    }
}
