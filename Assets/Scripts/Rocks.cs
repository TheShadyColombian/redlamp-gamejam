﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocks : MonoBehaviour {

    public Transform solidModel;
    public Transform fractureHolder;
    public float fractureForce;
    public float fractureRadius;
    public float upwardsModifier;
    public float startAngularVelocity;

    private Rigidbody rb;
    private Collider coll;
    private List<Rigidbody> fractures = new List<Rigidbody>();

    // Start is called before the first frame update
    void Start() {

        TryGetComponent(out rb);
        TryGetComponent(out coll);
        rb.angularVelocity = new Vector3(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180)) * startAngularVelocity;

        for (int i = 0; i < fractureHolder.childCount; i++)
            if (fractureHolder.GetChild(i).GetComponent<Rigidbody>()) {
                fractures.Add(fractureHolder.GetChild(i).GetComponent<Rigidbody>());
                fractures[fractures.Count - 1].isKinematic = true;
            }

        fractureHolder.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update() {

    }

    public void Fracture () {

        coll.enabled = false;

        rb.isKinematic = true;

        rb.velocity = Vector3.zero;

        solidModel.gameObject.SetActive(false);
        fractureHolder.gameObject.SetActive(true);

        //Enable physics in fracture chunks
        foreach (Rigidbody b in fractures) {
            b.isKinematic = false;
            b.AddExplosionForce(fractureForce, transform.position, fractureRadius, upwardsModifier);
        }

    }

    private void OnCollisionEnter(Collision collision) {

        Fracture();

    }

}